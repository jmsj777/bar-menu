module Pages.NewTab exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

import Base exposing (Base, base)
import Browser exposing (Document)
import Browser.Navigation as Nav
import Error exposing (buildErrorMessage)
import Html exposing (Html, a, button, div, form, h1, h3, input, p, span, text)
import Html.Attributes exposing (class, href, type_)
import Html.Events exposing (onClick, onInput)
import Http
import Model.Tab as Tab exposing (Tab)
import Route
import Url exposing (Protocol(..))


type alias Model =
    { navKey : Nav.Key
    , tab : Tab
    , createError : Maybe String
    }


init : Nav.Key -> ( Model, Cmd Msg )
init navKey =
    ( initialModel navKey, Cmd.none )


initialModel : Nav.Key -> Model
initialModel navKey =
    { navKey = navKey
    , tab = Tab.empty
    , createError = Nothing
    }


view : Model -> Document Msg
view model =
    { title = "New Tab"
    , body =
        base
            (Base
                [ div [ class "left" ]
                    [ a
                        [ class "nav", href (Route.toString Route.ListTabs) ]
                        [ span [] [ text "<" ] ]
                    ]
                ]
                [ h1 [] [ text "New Tab" ]
                , p [] [ text "This is the new Tab Form." ]
                , viewNewForm
                , viewError model.createError
                ]
                [ div [ class "items" ]
                    [ button
                        [ class "item", onClick CreateTab ]
                        [ text "Save" ]
                    ]
                ]
            )
    }


viewNewForm : Html Msg
viewNewForm =
    form []
        [ div []
            [ text "Client Id"
            , input [ type_ "number", onInput StoreClientId ] []
            ]
        , div []
            [ text "Client Name"
            , input [ type_ "text", onInput StoreClientName ] []
            ]
        ]


viewError : Maybe String -> Html msg
viewError maybeError =
    case maybeError of
        Just error ->
            div []
                [ h3 [] [ text "Couldn't create a tab at this time." ]
                , text ("Error: " ++ error)
                ]

        Nothing ->
            text ""


type Msg
    = StoreClientName String
    | StoreClientId String
    | CreateTab
    | TabCreated (Result Http.Error Tab)
    | BadData String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StoreClientId id ->
            case String.toInt id of
                Just newId ->
                    let
                        oldTab =
                            model.tab

                        oldClient =
                            model.tab.client

                        newClient =
                            { oldClient | id = newId }

                        newTab =
                            { oldTab
                                | client = newClient
                            }
                    in
                    ( { model | tab = newTab }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        StoreClientName name ->
            let
                oldClient =
                    model.tab.client

                newClient =
                    { oldClient | name = name }

                oldTab =
                    model.tab

                newTab =
                    { oldTab
                        | client = newClient
                    }
            in
            ( { model | tab = newTab }, Cmd.none )

        CreateTab ->
            ( model, Tab.new model.tab TabCreated BadData )

        TabCreated (Ok tab) ->
            ( { model
                | tab = tab
              }
            , Route.pushUrl Route.ListTabs model.navKey
            )

        TabCreated (Err error) ->
            ( { model | createError = Just (buildErrorMessage error) }
            , Cmd.none
            )

        BadData string ->
            ( { model | createError = Just "Nome do cliente não pode ser vazio" }
            , Cmd.none
            )
