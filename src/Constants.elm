module Constants exposing (..)


protocol : String
protocol =
    "http"


host : String
host =
    "localhost"


portNum : Int
portNum =
    5019


server : String
server =
    protocol
        ++ "://"
        ++ host
        ++ ":"
        ++ String.fromInt portNum
