module Route exposing
    ( Route(..)
    , matchRoute
    , parseUrl
    , pushUrl
    , toString
    )

import Browser.Navigation as Nav
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, custom, map, oneOf, parse, s, top)


type Route
    = NotFound
    | ListTabs
    | EditTab Int
    | NewTab


parseUrl : Url -> Route
parseUrl url =
    case parse matchRoute url of
        Just route ->
            route

        Nothing ->
            NotFound


matchRoute : Parser (Route -> a) a
matchRoute =
    oneOf
        [ map ListTabs top
        , map EditTab
            (s "tab"
                </> (custom "TABID" <|
                        \tabId ->
                            String.toInt tabId
                    )
            )
        , map NewTab
            (s "tab" </> s "new")
        ]


pushUrl : Route -> Nav.Key -> Cmd msg
pushUrl route navKey =
    toString route
        |> Nav.pushUrl navKey


toString : Route -> String
toString route =
    case route of
        NotFound ->
            "/not-found"

        NewTab ->
            "/tab/new"

        EditTab tabId ->
            "/tab/" ++ String.fromInt tabId

        ListTabs ->
            "/"
