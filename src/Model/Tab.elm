module Model.Tab exposing
    ( Tab
    , decoder
    , empty
    , encoder
    , fetch
    , fetchAll
    , new
    , save
    , view
    , viewAll
    , viewNew
    )

import Constants
import Html as Html exposing (Html, a, div, text)
import Html.Attributes exposing (class, href)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode
import Model.Person as Person exposing (Person)
import Model.Selection as Selection exposing (Selection)
import RemoteData exposing (RemoteData(..))
import Route
import Url exposing (Protocol(..))


type alias Tab =
    { id : Int
    , client : Person
    , selection : List Selection
    }


empty : Tab
empty =
    { id = -1
    , client = Person.empty
    , selection = []
    }


encoder : Tab -> Encode.Value
encoder tab =
    Encode.object
        [ ( "id", Encode.int tab.id )
        , ( "client", Person.encoder tab.client )
        , ( "selection", Encode.list Selection.encoder tab.selection )
        ]


newEncoder : Tab -> Encode.Value
newEncoder tab =
    Encode.object
        [ ( "client", Person.encoder tab.client )
        , ( "selection", Encode.list Selection.encoder tab.selection )
        ]


tabsDecoder : Decoder (List Tab)
tabsDecoder =
    Decode.list decoder


decoder : Decoder Tab
decoder =
    Decode.succeed Tab
        |> required "id" Decode.int
        |> required "client" Person.decoder
        |> required "selection" (Decode.list Selection.decoder)


fetch : Int -> (Result Http.Error Tab -> msg) -> Cmd msg
fetch clientId msg =
    Http.get
        { url = Constants.server ++ "/tabs/" ++ String.fromInt clientId
        , expect =
            decoder
                |> Http.expectJson msg
        }


fetchAll : (Result Http.Error (List Tab) -> msg) -> Cmd msg
fetchAll msg =
    Http.get
        { url = Constants.server ++ "/tabs"
        , expect =
            tabsDecoder
                |> Http.expectJson msg
        }


viewAll : List Tab -> Html msg
viewAll tabs =
    div [ class "box" ]
        [ div []
            [ div [ class "title" ] [ text "Lista de Comandas em aberto:" ]
            , div [ class "items" ] (List.map view tabs ++ [ viewNew ])
            ]
        ]


view : Tab -> Html msg
view tab =
    a
        [ class "item", href (Route.toString (Route.EditTab tab.id)) ]
        [ div [ class "name" ] [ text tab.client.name ]
        , div [ class "side-by-side" ]
            [ div []
                [ text ("#" ++ String.fromInt tab.id) ]
            , div
                [ class "totalPrice" ]
                [ Selection.totalPrice tab.selection ]
            ]
        ]


viewNew : Html msg
viewNew =
    a
        [ class "item new", href (Route.toString Route.NewTab) ]
        [ div [ class "name" ] [ text "NEW TAB" ]
        ]


new : Tab -> (Result Http.Error Tab -> msg) -> (String -> msg) -> Cmd msg
new tab action actionErr =
    if String.length tab.client.name > 0 then
        Http.post
            { url = Constants.server ++ "/tabs"
            , body = Http.jsonBody (newEncoder tab)
            , expect = Http.expectJson action decoder
            }

    else
        Cmd.map actionErr Cmd.none


save : Tab -> (Result Http.Error Tab -> msg) -> Cmd msg
save tab tabSaved =
    let
        filteredSelection =
            tab.selection
                |> List.filter (\l -> l.quantity > 0)

        tabToSave =
            { tab | selection = filteredSelection }
    in
    Http.request
        { method = "PUT"
        , headers = []
        , url = Constants.server ++ "/tabs/" ++ String.fromInt tab.client.id
        , body =
            Http.jsonBody
                (encoder tabToSave)
        , expect =
            Http.expectJson tabSaved decoder
        , timeout = Nothing
        , tracker = Nothing
        }
