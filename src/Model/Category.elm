module Model.Category exposing
    ( Category
    , fetchAll
    , viewAll
    )

import Constants
import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Model.Item as Item exposing (Item)


type alias Category =
    { id : Int
    , name : String
    , items :
        List Item
    }


categoriesDecoder : Decoder (List Category)
categoriesDecoder =
    Decode.list decoder


decoder : Decoder Category
decoder =
    Decode.succeed Category
        |> required "id" Decode.int
        |> required "name" Decode.string
        |> required "items" (Decode.list Item.decoder)


fetchAll : (Result Http.Error (List Category) -> msg) -> Cmd msg
fetchAll msg =
    Http.get
        { url = Constants.server ++ "/categories"
        , expect =
            categoriesDecoder
                |> Http.expectJson msg
        }


viewAll : List Category -> (String -> msg) -> Html msg
viewAll categories onclick =
    categories
        |> List.map (view onclick)
        |> div [ class "list-products" ]


view : (String -> msg) -> Category -> Html msg
view onclick l =
    div [ class "box" ] [ div [] [ text l.name ], Item.view l.items onclick ]
