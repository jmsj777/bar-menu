module Model.Item exposing
    ( Item
    , decoder
    , encoder
    , view
    )

import Html exposing (Html, button, div, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode
import Utils exposing (formatMoney)


type alias Item =
    { name : String
    , price : Float
    }


decoder : Decoder Item
decoder =
    Decode.succeed Item
        |> required "name" Decode.string
        |> required "price" Decode.float


encoder : Item -> Encode.Value
encoder item =
    Encode.object
        [ ( "name", Encode.string item.name )
        , ( "price", Encode.float item.price )
        ]


view : List Item -> (String -> msg) -> Html msg
view items onclick =
    items
        |> List.map
            (\l ->
                button [ class "item", onClick (onclick l.name) ]
                    [ div [ class "name" ] [ text l.name ]
                    , div [ class "val" ] [ text (formatMoney l.price) ]
                    ]
            )
        |> div [ class "items" ]
