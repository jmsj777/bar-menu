module Utils exposing (formatMoney, serverUrl)


serverUrl : String
serverUrl =
    "http://localhost:5019/"


formatMoney : Float -> String
formatMoney val =
    "R$ " ++ String.fromFloat val ++ ",00"
