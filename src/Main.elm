module Main exposing (main)

import Browser exposing (Document, UrlRequest)
import Browser.Navigation as Nav
import Html exposing (Html)
import Json.Decode as Decode
import Pages.EditTab as EditTab
import Pages.ListTabs as ListTabs
import Pages.NewTab as NewTab
import Route exposing (Route)
import Url exposing (Url)


main : Program Decode.Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


type Page
    = EditTabPage EditTab.Model
    | ListTabsPage ListTabs.Model
    | NewTabPage NewTab.Model
    | NotFoundPage


type alias Model =
    { route : Route
    , page : Page
    , navKey : Nav.Key
    }


init : Decode.Value -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url navKey =
    let
        ( listTabsModel, listTabsCmd ) =
            ListTabs.init flags

        model =
            { route = Route.parseUrl url
            , page = ListTabsPage listTabsModel
            , navKey = navKey
            }

        cmds =
            Cmd.map ListTabsMsg listTabsCmd
    in
    initCurrentPage ( model, cmds )


initCurrentPage : ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
initCurrentPage ( model, cmds ) =
    let
        ( newPage, newCmds ) =
            case model.route of
                Route.NotFound ->
                    ( NotFoundPage, Cmd.none )

                Route.ListTabs ->
                    let
                        ( pageModel, pageCmds ) =
                            ListTabs.init 0
                    in
                    ( ListTabsPage pageModel, Cmd.map ListTabsMsg pageCmds )

                Route.EditTab tabId ->
                    let
                        ( pageModel, pageCmds ) =
                            EditTab.init tabId model.navKey
                    in
                    ( EditTabPage pageModel, Cmd.map EditTabMsg pageCmds )

                Route.NewTab ->
                    let
                        ( pageModel, pageCmds ) =
                            NewTab.init model.navKey
                    in
                    ( NewTabPage pageModel, Cmd.map NewTabMsg pageCmds )
    in
    ( { model | page = newPage }, Cmd.batch [ newCmds, cmds ] )


type Msg
    = EditTabMsg EditTab.Msg
    | ListTabsMsg ListTabs.Msg
    | NewTabMsg NewTab.Msg
    | LinkClicked UrlRequest
    | UrlChanged Url


type alias Document msg =
    { title : String
    , body : List (Html msg)
    }


view : Model -> Document Msg
view model =
    case model.page of
        EditTabPage editTabModel ->
            let
                documentPage =
                    EditTab.view editTabModel

                body =
                    List.map (\e -> Html.map EditTabMsg e) documentPage.body
            in
            { title = documentPage.title
            , body = body
            }

        ListTabsPage listTabsModel ->
            let
                documentPage =
                    ListTabs.view listTabsModel

                body =
                    documentPage.body
                        |> List.map (\e -> Html.map ListTabsMsg e)
            in
            { title = documentPage.title
            , body = body
            }

        NewTabPage newTabModel ->
            let
                documentPage =
                    NewTab.view newTabModel

                body =
                    List.map (\e -> Html.map NewTabMsg e) documentPage.body
            in
            { title = documentPage.title
            , body = body
            }

        NotFoundPage ->
            notFoundView


notFoundView : Document Msg
notFoundView =
    { title = "404"
    , body =
        [ Html.div [] [ Html.text "Not found" ]
        ]
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( EditTabMsg subMsg, EditTabPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    EditTab.update subMsg pageModel
            in
            ( { model | page = EditTabPage updatedPageModel }
            , Cmd.map EditTabMsg updatedCmd
            )

        ( ListTabsMsg subMsg, ListTabsPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    ListTabs.update subMsg pageModel
            in
            ( { model | page = ListTabsPage updatedPageModel }
            , Cmd.map ListTabsMsg updatedCmd
            )

        ( NewTabMsg subMsg, NewTabPage pageModel ) ->
            let
                ( updatedPageModel, updatedCmd ) =
                    NewTab.update subMsg pageModel
            in
            ( { model | page = NewTabPage updatedPageModel }
            , Cmd.map NewTabMsg updatedCmd
            )

        ( LinkClicked urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model
                    , Nav.pushUrl model.navKey
                        (Url.toString url)
                    )

                Browser.External _ ->
                    ( model
                    , Cmd.none
                      -- Nav.load url
                    )

        ( UrlChanged url, _ ) ->
            let
                newRoute =
                    Route.parseUrl url
            in
            initCurrentPage
                ( { model | route = newRoute }, Cmd.none )

        ( _, _ ) ->
            ( model, Cmd.none )
