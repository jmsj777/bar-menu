module Base exposing
    ( Base
    , base
    , fromWebData
    )

import Error exposing (buildErrorMessage)
import Html exposing (Html, div, footer, h3, header, text)
import Html.Attributes exposing (class)
import RemoteData exposing (RemoteData(..), WebData)


type alias Base msg =
    { head : List (Html msg)
    , body : List (Html msg)
    , feet : List (Html msg)
    }


base : Base msg -> List (Html msg)
base { head, body, feet } =
    [ div
        [ class "app" ]
        [ header []
            head
        , div [ class "scroll" ]
            body
        , footer []
            (feet ++ [ div [] [ text "feito por ze @jmsj777" ] ])
        ]
    ]


fromWebData : WebData a -> (a -> Html msg) -> Html msg
fromWebData data callback =
    case data of
        NotAsked ->
            div [] [ text "Not asked..." ]

        Loading ->
            h3 [] [ text "Loading..." ]

        Success response ->
            callback response

        Failure err ->
            div []
                [ h3 [] [ text "Coudldn't fetch data at this time" ]
                , text ("Error: " ++ buildErrorMessage err)
                ]
