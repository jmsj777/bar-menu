# Elm Bar Menu

Here we have my study on elm, this is a tab app, where users can create a bar tab, and select it's items. The app also show the tabs stored in the database.

It is done based on some architectures from [elmprogramming.com](https://elmprogramming.com/) 's tutorial, and I'm using it just to train my elm and functional programming skills. I'm not using it for production, but it's a good example of how to use elm in an organized way.

## Technologies used

- json-server for the database
- Elm for the frontend
- Vanilla CSS for Styling

## Screenshots

Just so we get a glimpse of the app, here are some screenshots:

![imgur initial page](https://imgur.com/RnF0I9C.png)
![new tab](https://imgur.com/wDl88yk.png)
![edit tab](https://imgur.com/rqdrCAo.png)
![edit tab: selected items](https://imgur.com/iuLGy8M.png)
## Setup

To run app and server:

```
$ json-server --watch server/db.json -p 5019

$ elm-live src/Main.elm --pushstate -- --output=elm.js --debug
```